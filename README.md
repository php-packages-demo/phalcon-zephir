# phalcon-zephir

Compiled high level language aimed to the creation of C-extensions for PHP https://zephir-lang.com

### Installation
* v0.11 https://docs.zephir-lang.com/0.11/en/installation
* https://hub.docker.com/r/phalconphp/zephir